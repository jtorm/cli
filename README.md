# CLI

## Install

```js
// npm
npm install @jtorm/cli

// yarn
yarn add @jtorm/cli
```

## Setup
Register your cli command with parameters alias, pkg, action (function), params (param as key => description), description.
It is also possible to register events before and/or after the cli command with parameters when (before/after), alias, action (function).
