/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  cliEventModel = require('./../cli-event.model'),

  cliEventRegisterModel = (when, alias, action) => {
    if (!cliEventModel[when][alias])
      cliEventModel[when][alias] = []
    ;

    cliEventModel[when][alias].push(action);
  }
;

module.exports = {
  cliEventRegisterModel
};
