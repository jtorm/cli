/*! (c) jTorm and other contributors | https://jtorm.com/license */

const cliEventModel = {
  after: {},
  before: {}
};

module.exports = {
  cliEventModel
};
