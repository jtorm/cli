/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  cliModel = require('./../cli.model'),

  cliRegisterModel = (alias, pkg, action, params, description) => {
    cliModel[alias] = {
      action: action,
      description: description,
      package: pkg,
      params: params
    };
  }
;

module.exports = {
  cliRegisterModel
};
