/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { cliModel } = require('./../../../cli.model'),
  { cliEventModel } = require('./../../../cli-event.model'),

  cliActionModel = async () => {
    const argv = require('minimist')(process.argv.slice(2));

    if (argv.alias) {
      const
        alias = argv.alias,
        command = cliModel[alias],
        action = require(command.package)[command.action]
      ;

      console.log(command.description);

      if (cliEventModel.before[alias])
        await cliEventModel.before[alias].action(argv)
        ;

      await action(argv);

      if (cliEventModel.after[alias])
        await cliEventModel.after[alias].action(argv)
        ;
    } else {
      console.log('Available commands');
      console.log(' ');

      for (let alias in cliModel) {
        console.log(alias);
        console.log(cliModel[alias].description);
        console.log(' ');

        console.log('Parameters');
        console.log(' ');

        for (let param in cliModel[alias].params) {
          console.log(param);
          console.log(cliModel[alias].params[param]);
          console.log(' ');
        }

        console.log('-----');
        console.log(' ');
      }
    }

    process.exit(0);
  }
;

module.exports = {
  cliActionModel
};
