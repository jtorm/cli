/*! (c) jTorm and other contributors | https://jtorm.com/license */

const
  { cliModel } = require('./models/cli.model'),
  { cliEventModel } = require('./models/cli-event.model'),
  { cliRegisterModel } = require('./models/cli/cli-register.model'),
  { cliEventRegisterModel } = require('./models/cli-event/cli-event-register.model'),
  { cliActionModel } = require('./models/services/actions/cli/cli-action.model')
;

module.exports = {
  cliModel,
  cliEventModel,
  cliRegisterModel,
  cliEventRegisterModel,
  cliActionModel
};
